package com.yackback.reximus;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaRecorder;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.CompoundButton;
import android.widget.Toast;
import android.widget.ToggleButton;

import es.dmoral.toasty.Toasty;

public class MainActivity extends AppCompatActivity {
    Context mContext;
    private static final String TAG="Reximus/MainActivity";
    int PERMISSION_RECORD_AUDIO = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mContext = getApplicationContext();
        final SoundMeter mMeter = new SoundMeter(this);
        ToggleButton toggleButton = (ToggleButton) findViewById(R.id.main_content_toggleButton);
        toggleButton.setChecked(false);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                mMeter.setActivated(isChecked);
                if (isChecked) {
                    // The toggle is enabled
                    // Check for microphone permission
                    handleMicrophonePermission();
                    Intent intent = new Intent(mContext, VolumeService.class);
                    startService(intent);
                    Toasty.success(mContext,
                            getString(R.string.toast_message_toggleOn),
                            Toast.LENGTH_SHORT, true)
                            .show();
                } else {
                    // The toggle is disabled
                    mMeter.stop();
                    Toasty.success(mContext,
                            getString(R.string.toast_message_toggleOff),
                            Toast.LENGTH_SHORT, true)
                            .show();
                }
            }
        });
    }

    private void handleMicrophonePermission() {
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED) {
            boolean permission = ( ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED );
            Log.d(TAG, String.valueOf(permission));
            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this,
                    Manifest.permission.RECORD_AUDIO)) {
                Toasty.info(this, getString(R.string.info_explain_audio_permission),
                        Toast.LENGTH_SHORT, true).show();
            } else {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSION_RECORD_AUDIO);
            }
        }
    }

    // TODO: onRequestPermissionsResult
    /*
    @Override
    public void onRequestPermissionsResult() {


    }
    */
}
